package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState [] [] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState [rows] [columns];
        for (int row = 0; row < rows; row++){
            for (int col = 0; col < this.cols; col ++) {
                this.grid[row] [col] = initialState;

            }
        }
    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
       
        this.grid[row] [column] = element;

    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return this.grid[row] [column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid theCopy = new CellGrid(this.rows, this.cols, null);
        for (int row = 0; row < this.rows; row++){
            for (int col = 0; col < this.cols; col++){
                theCopy.set(row, col, this.get(row, col));
                theCopy.grid[row] [col] = this.grid[row] [col];
                
            }
        }
        return theCopy;
    }
    
}
